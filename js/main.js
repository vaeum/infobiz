var app = {
    init: function () {
        this.fixMenu();         //таймер
        this.callBack();        //обратный звонок
        this.firstEnterForm();  //первая форма
        this.firstEnterForm2(); //вторая форма
        this.firstEnterForm3(); //третья форма
        this.firstEnterForm4(); //четвертая форма
        this.zakaz();           //Кнопка заказать
        this.galleries();       //Галлереи
    },
    
    /*Таймер*/
    fixMenu: function () {
        setCountDate();  
    },
    
    /*обратный звонок*/
    callBack: function () {
        $("#callback").click(function(){  
           $(".popup-one").fadeIn();
           $(".modal_wrap").fadeIn(300);
           return false; 
        });
        
        $("#close1").click(function(){
           $(".popup-one").fadeOut(200);
           $(".modal_wrap").fadeOut(300);
           return false;  
        });
        
        /*обязательное заполнение*/
        $("#callback_form1").on('submit',function(){
            var name = $("#name1"),
                city = $("#city1"),
                phone = $("#phone1"),
                tel_Regex = new RegExp('[а-яА-ЯёЁa-zA-Z]'),
                error = 0;
                                
                if(name.val() == ""){
                    name.css({"box-shadow": "inset 0px 0px 10px #ff0000", "border-color": "#ff0000"});
                    error = 1;
                }else{
                    name.css({"box-shadow": "none", "border-color": "#ffffff"});                    
                }
                
                if(city.val() == ""){
                    city.css({"box-shadow": "inset 0px 0px 10px #ff0000", "border-color": "#ff0000"});
                    error = 1;
                }else{
                    city.css({"box-shadow": "none", "border-color": "#ffffff"});                    
                }
                
                if (true == tel_Regex.test(phone.val()) || phone.val() == "") {
                    phone.css({"box-shadow": "inset 0px 0px 10px #ff0000", "border-color": "#ff0000"});
                    error = 1;
                }else{
                    phone.css({"box-shadow": "none", "border-color": "#ffffff"});                    
                }             
                
                if(error == 0){
                    $(this).submit();                  
                }else{
                    return false;
                }
        });
    },
    
    /*первая форма ввода*/
    firstEnterForm: function(){
        //проверка ввода
        $("#enter_form1").on('submit',function(){
            if(stop == 1){ return false;}
            
            var name = $("#name2"),
                email = $("#email2"),
                email_Regex = new RegExp("^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$"),
                error = 0,
                url = "http://programmbiz.e-autopay.com/order1/54140";
                
                if($("#r2").is(':checked')){
                    url = "http://programmbiz.e-autopay.com/order1/54123";
                }
                
                if(name.val() == ""){
                    name.css({"box-shadow": "inset 0px 0px 10px #ff0000", "border-color": "#ff0000"});
                    error = 1;
                }else{
                    name.css({"box-shadow": "none", "border-color": "#ffffff"});                    
                }
                
                if (email_Regex.test(email.val()) == false) {
                    email.css({"box-shadow": "inset 0px 0px 10px #ff0000", "border-color": "#ff0000"});
                    error = 1;
                }else{
                    email.css({"box-shadow": "none", "border-color": "#ffffff"});                    
                }            
                
                if(error == 0){   
                        stop = 1; 
                    var win=window.open(url, '_blank');
                        win.focus();
                    $(this).submit();      
                }else{
                    return false;
                }
        });
    },
    
    /*вторая форма ввода*/
    firstEnterForm2: function(){
        //проверка ввода
        $("#enter_form2").on('submit',function(){
            if(stop == 1){ return false;}
            
            var name = $("#name3"),
                email = $("#email3"),
                email_Regex = new RegExp("^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$"),
                error = 0,
                url = "http://programmbiz.e-autopay.com/order1/54140";
                
                if($("#r4").is(':checked')){
                    url = "http://programmbiz.e-autopay.com/order1/54123";
                }
                
                
                if(name.val() == ""){
                    name.css({"box-shadow": "inset 0px 0px 10px #ff0000", "border-color": "#ff0000"});
                    error = 1;
                }else{
                    name.css({"box-shadow": "none", "border-color": "#ffffff"});                    
                }
                
                if (email_Regex.test(email.val()) == false) {
                    email.css({"box-shadow": "inset 0px 0px 10px #ff0000", "border-color": "#ff0000"});
                    error = 1;
                }else{
                    email.css({"box-shadow": "none", "border-color": "#ffffff"});                    
                }            
                
                if(error == 0){   
                        stop = 1; 
                    var win=window.open(url, '_blank');
                        win.focus();
                    $(this).submit();      
                }else{
                    return false;
                }
        });
    },
    
    /*третья форма ввода*/
    firstEnterForm3: function(){
        //проверка ввода
        $("#enter_form3").on('submit',function(){           
            if(stop == 1){ return false;}
            
            var name = $("#name4"),
                email = $("#email4"),
                email_Regex = new RegExp("^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$"),
                error = 0,
                url = "http://programmbiz.e-autopay.com/order1/54140";
                
                if($("#r6").is(':checked')){
                    url = "http://programmbiz.e-autopay.com/order1/54123";
                }
                
                
                if(name.val() == ""){
                    name.css({"box-shadow": "inset 0px 0px 10px #ff0000", "border-color": "#ff0000"});
                    error = 1;
                }else{
                    name.css({"box-shadow": "none", "border-color": "#ffffff"});                    
                }
                
                if (email_Regex.test(email.val()) == false) {
                    email.css({"box-shadow": "inset 0px 0px 10px #ff0000", "border-color": "#ff0000"});
                    error = 1;
                }else{
                    email.css({"box-shadow": "none", "border-color": "#ffffff"});                    
                }            
                
                if(error == 0){      
                    stop = 1;  
                    window.open(url, '_blank');
                    $(this).submit();                       
                    return true;
                }else{
                    return false;
                }
            
        });
    },
    
    /*четвертая форма ввода*/
    firstEnterForm4: function(){
        //проверка ввода
        $("#enter_form4").on('submit',function(){           
            if(stop == 1){ return false;}
            
            var name = $("#name5"),
                email = $("#email5"),
                email_Regex = new RegExp("^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$"),
                error = 0,
                url = "http://programmbiz.e-autopay.com/order1/54140";
                
                if($("#r8").is(':checked')){
                    url = "http://programmbiz.e-autopay.com/order1/54123";
                }
                
                
                if(name.val() == ""){
                    name.css({"box-shadow": "inset 0px 0px 10px #ff0000", "border-color": "#ff0000"});
                    error = 1;
                }else{
                    name.css({"box-shadow": "none", "border-color": "#ffffff"});                    
                }
                
                if (email_Regex.test(email.val()) == false) {
                    email.css({"box-shadow": "inset 0px 0px 10px #ff0000", "border-color": "#ff0000"});
                    error = 1;
                }else{
                    email.css({"box-shadow": "none", "border-color": "#ffffff"});                    
                }            
                
                if(error == 0){      
                    stop = 1;  
                    window.open(url, '_blank');
                    $(this).submit();                       
                    return true;
                }else{
                    return false;
                }
            
        });
    },
    
    zakaz: function(){
        $(".zakaz").click(function(){
            //$('body,html').animate({scrollTop:20},1400);
            $(".modal_wrap2").fadeIn(400);
            return false
        });
        $("#close2").click(function(){
            $(".modal_wrap2").fadeOut(300);
            return false;
        });
    },
    
    galleries: function(){
        $(".fancy1").fancybox();
        $(".fancy2").fancybox();
    }       
      
    
}

var stop = 0; 

jQuery(document).ready(function() {    
    //Инициализируем
    app.init();    
});

var year, 
    month,
    day,
    hour,
    minute,
    sec,
    dateFuture; 
    
function setCountDate(){
    $.ajax({
		url: 'incl/actiontimer/settime.php',
		type: 'post',
		data: "takedate=1",
        success: function(data) {  
            year = data.year; 
            month = data.month;            
            day = data.day; 
            hour= data.hour; 
            minute= data.min; 
            sec= data.sec;             
            
            month = --month;
            dateFuture = new Date(year, month, day, hour, minute, sec);
            CountBox();              
		}
	});    
}

function CountBox() {
    dateNow = new Date;    
    amount = dateFuture.getTime() - dateNow.getTime() + 5;
    delete dateNow;
    if (amount < 0) {
        $("#action_day1, #action_day2").html("00");
        $("#action_hour1, #action_hour2").html("00");
        $("#action_min1, #action_min2").html("00");
        $("#action_sec1, #action_sec2").html("00");
    } else {
      	days = 0;
        days1 = 0;
        days2 = 0;
        hours = 0;
        hours1 = 0;
        hours2 = 0;
        mins = 0;
        mins1 = 0;
        mins2 = 0;
        secs = 0;
        secs1 = 0;
        secs2 = 0;
        out = "";
        amount = Math.floor(amount / 1e3);
        days = Math.floor(amount / 86400);
        days1 = (days >= 10) ? days.toString().charAt(0) : '0';
        days2 = (days >= 10) ? days.toString().charAt(1) : days.toString().charAt(0);
        amount = amount % 86400;
        hours = Math.floor(amount / 3600);
        hours1 = (hours >= 10) ? hours.toString().charAt(0) : '0';
        hours2 = (hours >= 10) ? hours.toString().charAt(1) : hours.toString().charAt(0);
        amount = amount % 3600;
        mins = Math.floor(amount / 60);
        mins1 = (mins >= 10) ? mins.toString().charAt(0) : '0';
        mins2 = (mins >= 10) ? mins.toString().charAt(1) : mins.toString().charAt(0);
        amount = amount % 60;
        secs = Math.floor(amount);
        secs1 = (secs >= 10) ? secs.toString().charAt(0) : '0';
        secs2 = (secs >= 10) ? secs.toString().charAt(1) : secs.toString().charAt(0);
        
        $("#action_day1, #action_day2, #action_day3").html(days1+days2);
        $("#action_hour1, #action_hour2, #action_hour3").html(hours1+hours2);
        $("#action_min1, #action_min2, #action_min3").html(mins1+mins2);
        $("#action_sec1, #action_sec2, #action_sec3").html(secs1+secs2);
                
        setTimeout("CountBox()", 1e3)
    }
}