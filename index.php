<?php include "header.php" ?>
<?php include "metrika.php" ?>
<?php include "consultant.php" ?>
<?php include( "incl/saveform/save.php"); ?>


<div class="header-top">
	<div class="container2">
		<div class="row">
			<div class="col-md-3">
				<img src="img/logo1.jpg" alt="" class="left-img">
			</div>
			<div class="col-md-6">
				<p>Ступенчатая система обучения инфобизнесу без начального капитала</p>
			</div>
			<div class="col-md-3">
				<img src="img/logo2.png" alt="" class="right-img">
			</div>
		</div>
	</div>
</div>

<div class="modal_wrap">
	<div class="popup-one" style="display: none;">
		<img src="img/close.png" class="close_img" id="close1" />
		<div class="popup-head"></div>
		<form action="" id="callback_form1" method="POST">
			<input type="hidden" name="send_form1" value="1" />
			<input type="text" id="name1" name="username" placeholder="Имя">
			<input type="text" id="phone1" name="phone" placeholder="Телефон">
			<input type="text" id="city1" name="city" placeholder="Город">
			<input type="submit" onclick="yaCounter24857429.reachGoal('ZVONOK'); return true;" value="Заказать звонок">
			<p>*Ваши данные в безопасности и не будут переданы третьим лицам.</p>
		</form>
	</div>

	<?php if(isset($_POST['send_form1'])) { ?>
	<div class="popup-two" style="display: none;">
		<div class="popup-head"></div>
		<i></i>
		<p>Спасибо!</p>
		<p>Ваша заявка принята.
			<br>Наши менеджеры свяжутся
			<br>с Вами в ближайшее время.</p>
		<!--<a href="#" onClick="$('.modal_wrap').fadeOut(300); $('.popup-two').fadeOut(); return false;">Закрыть</a>-->
		<a href="http://programmbiz.ru/">Закрыть</a>
	</div>
	<script>
		jQuery(document).ready(function($) {
			//Инициализируем
			$(".popup-two").fadeIn(300);
			$(".modal_wrap").fadeIn(300);
		});
	</script>
	<?php }?>

	<?php if(isset($_POST['send_enter_form'])) { ?>
	<div class="popup-two" style="display: none;">
		<div class="popup-head"></div>
		<i></i> 
		<p>Спасибо за Ваш заказ!</p>
<!--		<a href="#" onClick="$('.modal_wrap').fadeOut(300); $('.popup-two').fadeOut(); return false;">Закрыть</a>-->
	<a href="http://programmbiz.ru/">Закрыть</a>
	</div>
	<script>
		jQuery(document).ready(function($) {
			//Инициализируем
			$(".popup-two").fadeIn(300);
			$(".modal_wrap").fadeIn(300);
		});
	</script>
	<?php }?>
</div>

<div class="modal_wrap2">
	<div class="head-form2">
		<img src="img/close.png" class="close_img" id="close2" />
		<div class="head-form-top">
			<p>Закажите полный курс</p>
		</div>
		<form action="" id="enter_form4" method="POST" class="main-form" onsubmit="yaCounter24857429.reachGoal('SREDFORMA'); return true;">
			<input type="hidden" name="send_enter_form" value="1" />
			<input type="text" id="name5" name="username" placeholder="Имя">
			<input type="text" id="email5" name="email" placeholder="E-mail">
			<input type="radio" id="r7" name="rr" value="electro" checked>
			<label for="r7"><span></span>Электронная версия</label>
			<input type="radio" id="r8" name="rr" value="cd">
			<label for="r8"><span></span>Версия на диске</label>
			<input type="submit" value="Заказать курс">
			<p>*Ваши данные в безопасности и не будут переданы третьим лицам.</p>
		</form>
	</div>
</div>

<div class="header">
	<div class="container2">
		<div class="row head-top">
			<div class="col-md-12">
				<h1>инфобизнес 3.0</h1>
				<p><b>97</b>  уроков, <b>6 Гб</b> уникального видео, <b>24</b> часа обучающего материала</p>
			</div>
			<a href="" class="yellow-button" id="callback">Заказать звонок</a>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="head-akcia">
					<p class="red1">акция</p>
					<p>только 3 дня</p>
					<p class="red2">- 70%</p>
					<div class="row">
						<div class="col-xs-6">
							<em>7 600р</em>
							<p class="red3">2 300 р</p>
							<p class="normal-text">электронная версия</p>
						</div>
						<div class="col-xs-6">
							<em>9 990р</em>
							<p class="red3">2 990 р</p>
							<p class="normal-text">версия
								<br>на диске</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-3">
							<div class="counter-bg">
								<p id="action_day1">00</p>
							</div>
							<i>дней</i>
						</div>
						<div class="col-xs-3">
							<div class="counter-bg">
								<p id="action_hour1">00</p>
							</div>
							<i>часов</i>
						</div>
						<div class="col-xs-3">
							<div class="counter-bg">
								<p id="action_min1">00</p>
							</div>
							<i>минут</i>
						</div>
						<div class="col-xs-3">
							<div class="counter-bg">
								<p id="action_sec1">00</p>
							</div>
							<i>секунд</i>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="head-form">
					<div class="head-form-top">
						<p>Закажите полный курс</p>
					</div>
					<div class="main-form">
						<form action="" id="enter_form1" method="POST" onsubmit="yaCounter24857429.reachGoal('VERHFORMA'); return true;">
							<input type="hidden" name="send_enter_form" value="1" />
							<input type="text" id="name2" name="username" placeholder="Имя">
							<input type="text" id="email2" name="email" placeholder="E-mail">
							<input type="radio" id="r1" name="rr" value="electro" checked>
							<label for="r1"><span></span>Электронная версия</label>
							<input type="radio" id="r2" name="rr" value="cd">
							<label for="r2"><span></span>Версия на диске</label>
							<input type="submit" value="Заказать курс">
							<p>*Ваши данные в безопасности и не будут переданы третьим лицам.</p>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="chto-takoe">
	<div class="container">
		<h2>Что такое инфобизнес?</h2>
		<p>Инфобизнес - это бизнес по продаже информации (знаний), которая несет в себе какую-то полезную составляющую и эту информацию как-то можно применить. То есть в настоящем инфобизнесе вы не сможете продать информацию о том, что глубина Азовского моря не более 15 метров, и это самое мелкое море в мире, но вы можете продать какое-то руководство, как в это море погружаться или заниматься дайвингом. Люди осознали, что инфобизнес может приносить высокий доход. Информация может быть оформлена в любом законченном виде: статья, книга, программа, обучающий курс, аудио и видео контент и т.п.</p>
	</div>
</div>

<div class="fakti">
	<div class="top"></div>
	<div class="container">
		<h2>Некоторые факты</h2>
		<div class="row">
			<div class="col-md-3">
				<img src="img/icon-1.png" alt="">
				<p>Опыт ведения
					<br>инфобизнеса <b>8 лет</b>
				</p>
			</div>
			<div class="col-md-3">
				<img src="img/icon-2.png" alt="">
				<p><b>Около 100 000</b>
					<br>благодарных пользователей</p>
			</div>
			<div class="col-md-3">
				<img src="img/icon-3.png" alt="">
				<p><b>7 853</b>
					<br>положительных отзыва, присланных по e-mail</p>
			</div>
			<div class="col-md-3">
				<img src="img/icon-4.png" alt="">
				<p><b>7</b>
					<br>человек стали миллионерами</p>
			</div>
		</div>
	</div>
	<div class="bot"></div>
</div>

<div class="predlogaem">
	<div class="container">
		<h2>Что мы предлагаем</h2>
		<p>Вы получите новую высокодоходную профессию.
			<br>(Пройдя курс, средний доход инфобизнесмена составляет 300 000 руб./мес)</p>
		<div class="row">
			<div class="col-md-4">
				<img src="img/1-icon.jpg" alt="">
			</div>
			<div class="col-md-4">
				<img src="img/2-icon.jpg" alt="">
			</div>
			<div class="col-md-4">
				<img src="img/3-icon.jpg" alt="">
			</div>
		</div>
		<a href="" class="podrobnee" data-toggle="modal" data-target="#сollapse">Блок 1. «Лучшие сервисы для инфобизнеса»</a>
		<a href="" class="podrobnee" data-toggle="modal" data-target="#сollapse2">Блок 2. «Лучшие программы для инфобизнеса»</a>
		<ul>
			<li>- содержит просто огромный массив полезной информации, более, чем на 6 Гб памяти</li>
			<li>- описывает порядка 25 сервисов и до 20 программ для инфобизнесмена</li>
			<li>- удобно структурирован по разделам и пунктам, записан последовательно и системно</li>
			<li>- создан практиками, которые хорошо разбираются в технических моментах инфобизнеса
				<br>и обучают людей компьютерной и интернет-грамотности</li>
			<li>- записан в высоком (HD) качестве, наглядно, хорошо, подробно, без «воды», понятным языком</li>
		</ul>
		<a href="" class="zakaz hidden-xs">Заказать</a>
		<div class="mobile-view visible-xs"><a href="" class="zakaz">Заказать</a>
		</div>
	</div>
</div>

<div class="chto-dast">
	<div class="top"></div>
	<div class="container">
		<h2>Что даст вам этот курс</h2>

		<div class="row">
			<div class="col-md-6">
				<img src="img/1.png" alt="">
				<h3>Вы научитесь работать с подписчиками</h3>
				<p>В сервисе рассылок №1 в Рунете — Smartresponder.ru, через который вы и будете продавать свои инфопродукты</p>
			</div>
			<div class="col-md-6">
				<img src="img/2.png" alt="">
				<h3>Вы разберетесь в системе Webmoney</h3>
				<p>Через которую будете получать часть прибыли — возможно и основную</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<img src="img/3.png" alt="">
				<h3>Вы научитесь эффективно планировать свою работу</h3>
				<p>Онлайн и создавать маркетинговые опросы</p>
			</div>
			<div class="col-md-6">
				<img src="img/4.png" alt="">
				<h3>Вы научитесь легко работать с видео</h3>
				<p>На крупнейшем видеохостинге Youtube, без которого не обойтись ни одному инфобизнесмену</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<img src="img/5.png" alt="">
				<h3>Вы подберете сервис для автоматизации вашего инфобизнеса</h3>
				<p>По части доставки ваших дисков покупателям и приема оплаты — для повышения продаж и избавления от рутины</p>
			</div>
			<div class="col-md-6">
				<img src="img/6.png" alt="">
				<h3>Вы подберете для своих сайтов бесплатный чат поддержки</h3>
				<p>Это увеличит лояльность ваших подписчиков и принесет дополнительную прибыль</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<img src="img/7.png" alt="">
				<h3>Вы выберете хостинг</h3>
				<p>Для ваших полноценных и мини-сайтов, оптимальный по цене и надежности</p>
			</div>
			<div class="col-md-6">
				<img src="img/8.png" alt="">
				<h3>Вы создадите красивые страницы подписки</h3>
				<p>Привлечете на них целевых посетителей, которые станут вашими подписчиками и клиентами</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<img src="img/9.png" alt="">
				<h3>Вы значительно упростите свою онлайн-бухгалтерию</h3>
				<p>Сэкономите много времени и нервов с сервисом «Мое дело»</p>
			</div>
			<div class="col-md-6">
				<img src="img/10.png" alt="">
				<h3>Вы будете создавать красивые и наглядные интеллект-карты</h3>
				<p>Их можно продавать как недорогие front-end товары</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<img src="img/11.png" alt="">
				<h3>Вы научитесь</h3>
				<p>Устанавливать удобные и красивые программы для просмотра любых типов файлов, а также электронных книг</p>
			</div>
			<div class="col-md-6">
				<img src="img/12.png" alt="">
				<h3>Вы узнаете</h3>
				<p>Как восстанавливать свои данные в случае их возможной утери</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<img src="img/13.png" alt="">
				<h3>Вы сумеете</h3>
				<p>Бесплатно отправлять SMS и MMS c любых телефонов и сервисов</p>
			</div>
			<div class="col-md-6">
				<img src="img/14.png" alt="">
				<h3> В дополнение к сервису</h3>
				<p>Вы изучите хорошие программы для планирования времени и дел, работы с Twitter, конвертации файлов и создания интеллект-карт и <i>многое другое в одном месте!</i>
				</p>
			</div>
		</div>
	</div>
	<div class="bot"></div>
</div>

<div class="skolko-stoit">
	<h2>Сколько это стоит</h2>
	<div class="stoit-form">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="head-akcia">
						<p class="red1">акция</p>
						<p>только 3 дня</p>
						<p class="red2">- 70%</p>
						<div class="row">
							<div class="col-xs-6">
								<em>7 600р</em>
								<p class="red3">2 300 р</p>
								<p class="normal-text">электронная версия</p>
							</div>
							<div class="col-xs-6">
								<em>9 990р</em>
								<p class="red3">2 990 р</p>
								<p class="normal-text">версия
									<br>на диске</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="counter-bg">
									<p id="action_day2">00</p>
								</div>
								<i>дней</i>
							</div>
							<div class="col-xs-3">
								<div class="counter-bg">
									<p id="action_hour2">00</p>
								</div>
								<i>часов</i>
							</div>
							<div class="col-xs-3">
								<div class="counter-bg">
									<p id="action_min2">00</p>
								</div>
								<i>минут</i>
							</div>
							<div class="col-xs-3">
								<div class="counter-bg">
									<p id="action_sec2">00</p>
								</div>
								<i>секунд</i>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="head-form">
						<div class="head-form-top">
							<p>Закажите полный курс</p>
						</div>
						<form action="" id="enter_form2" method="POST" class="main-form">
							<input type="hidden" name="send_enter_form" value="1" />
							<input type="text" id="name3" name="username" placeholder="Имя">
							<input type="text" id="email3" name="email" placeholder="E-mail">
							<input type="radio" id="r3" name="rr" value="electro" checked>
							<label for="r3"><span></span>Электронная версия</label>
							<input type="radio" id="r4" name="rr" value="cd">
							<label for="r4"><span></span>Версия на диске</label>
							<input type="submit" value="Заказать курс">
							<p>*Ваши данные в безопасности и не будут переданы третьим лицам.</p>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="besplat-pril">
	<h2>Бесплатные приложения к курсу</h2>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<img src="img/gift-1.jpg" alt="">
			</div>
			<div class="col-md-9">
				<h3>Сервис E-Autopay</h3>
				<p>Сервис E-Autopay — один из ведущих сервисов в этих направлениях. Он позволяет создавать продукты для продажи и апселлы к ним, принимать оплату самыми разными способами, чаще автоматически, учитывать информацию о заказах и клиентах, партнерские переходы и продажи и многое другое.
					<br><i>Цена курса - 2 500 рублей</i>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<img src="img/gift-2.jpg" alt="">
			</div>
			<div class="col-md-9">
				<h3>Мир электронных денег</h3>
				<p>Чтобы настроить оплату, помимо сервиса E-autopay, вам нужно хорошо знать и сами платежные системы. Если в основном курсе мы разобрали только Webmoney, то бонус научит вас большему.
					<br><i>Цена курса - 1 000 рублей</i>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<img src="img/gift-3.jpg" alt="">
			</div>
			<div class="col-md-9">
				<h3>200 каст великих бизнесменов 19-20 веков</h3>
				<p>Это специальный 55-минутный каст, озвученный в студии профессиональным диктором. Из него вы узнаете много мудрых мыслей и схем, которые позволят Вам вести инфобизнес более рационально и опережая конкурентов.
					<br><i>Цена курса - 500 рублей</i>
				</p>
			</div>
		</div>
	</div>
</div>

<div class="garantii">
	<div class="top"></div>
	<div class="container">
		<h2>Наши гарантии</h2>
		<div class="row">
			<div class="col-md-4">
				<img src="img/g-1.jpg" alt="">
			</div>
			<div class="col-md-8">
				<p>100% гарантия возврата денег в случае недовольства качеством.
					<br>(Данная гарантия действительна 365 дней с момента заказа).</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<img src="img/g-2.jpg" alt="">
			</div>
			<div class="col-md-8">
				<p>100% гарантия доставки курса в идеальном состоянии. (Для транспортировки курсов используются надежные коробки, тираж курса осуществляется на заводе и упаковываeтся в профессиональную упаковку DigiPack – в точно такие же красивые коробки упаковываются лицензионные фильмы. Диск находится под двойной защитой от внешних воздействий и жестко закреплен внутри бокса).</p>
			</div>
		</div>
	</div>
	<div class="bot"></div>
</div>

<div class="govorit-klient">
	<h2>Что о нас говорят клиенты</h2>
	<div class="container2">
		<div class="row">
			<div class="col-md-6">
				<img src="img/otzv-1.jpg" alt="">
				<h3>Константин Фёст</h3>
				<p>Учитывая имеющийся в курсе «Лучшие сервисы и программы для инфобизнеса» объем информации и ее качество, я считаю его одним из лучших вложений, которые сегодня может сделать <a href="" data-toggle="modal" data-target="#otziv1">Подробнее</a>
				</p>
			</div>
			<div class="col-md-6">
				<img src="img/otzv-2.jpg" alt="">
				<h3>Денисенко Николай</h3>
				<p>Являюсь коммерческим директором веб студии, используем курс по «Инфобизнесу» в качестве повышения навыков сотрудников. Курс подойдет как новичкам, так и профессионалам</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<img src="img/otzv-3.jpg" alt="">
				<h3>Гончаров Сергей</h3>
				<p>Мое основное направление - это продажи в интернете. Считаю, что каждый, кто действительно хочет правильно строить именно этот бизнес, должен владеть всей информацией по инфобизнесу. Я увеличил свой доход в 17 раз благодаря курсу, спасибо создателям!</p>
			</div>
			<div class="col-md-6">
				<img src="img/otzv-4.jpg" alt="">
				<h3>Максим Анисимов</h3>
				<p>Я научился как правильно делать видео уроки, презентации. Понравилась простота и пошаговая инструкция. Думаю, каждый найдет в нем что-то для себя</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<img src="img/otzv-5.jpg" alt="">
				<h3>Копычко Виктор</h3>
				<p>Сомневался на счет курса, все же приобрел, теперь у меня больше времени свободного, а все потому, что я большую часть работы делегировал другим. Не разочаровался. Создателям спасибо</p>
			</div>
			<div class="col-md-6">
				<img src="img/otzv-6.jpg" alt="">
				<h3>Александр Гусев</h3>
				<p>Мне посоветовал данный видеокурс коллега. Я расширяю свой бизнес, поэтому нужны были знания, как привлечь клиентов, какие технические, маркетинговые моменты нужно учесть. <a href="" data-toggle="modal" data-target="#otziv2">Подробнее</a>
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<img src="img/otzv-7.jpg" alt="">
				<h3>Иван Макаров</h3>
				<p>Я был сильно мотивирован заработком 300 000 руб/мес. Были кредиты, которые сдавливали душу. Нужно был срочно что-то решать. И я рискнул купить курс Дмитрия и Василия и пахать по 25 часов в сутки! <a href="" data-toggle="modal" data-target="#otziv3">Подробнее</a>
				</p>
			</div>
			<div class="col-md-6">
				<img src="img/otzv-8.jpg" alt="">
				<h3>Наталья Капустина</h3>
				<p>Я консультант и бизнес тренер. Заработано с тренинга 350 000 дохода за 6 месяцев работы. Оплата идет по 50 000 рублей в месяц. <a href="" data-toggle="modal" data-target="#otziv4">Подробнее</a>
				</p>
			</div>
		</div>
	</div>
</div>

<div class="kto-mi">
	<div class="top"></div>
	<div class="container">
		<h2>Кто мы?</h2>
		<div class="row">
			<div class="col-md-4">
				<img src="img/team-1.jpg" alt="">
			</div>
			<div class="col-md-8">
				<p><b>Дмитрий Юдин</b>
					<br>Инфобизнесмен с 3-х-летним стажем ведения интернет-бизнеса, автор многочисленных курсов по компьютерной и интернет-грамотности, создатель сайтов infodengy.ru, bookbyvoice.ru, pc-vestnik.ru. В подписной базе Дмитрия более 60 000 благодарных читателей.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<img src="img/team-2.jpg" alt="">
			</div>
			<div class="col-md-8">
				<p><b>Василий Купчихин</b>
					<br>Инфобизнесмен с 5-летним опытом ведения интернет-бизнеса. За это время выпустил 9 обучающих видеокурсов и собрал более 45 000 подписчиков. Досконально изучил все технические вопросы, связанные с ведением инфобизнеса и запуском новых бизнес-проектов. Является автором сайта blog.pc-lessons.ru.</p>
			</div>
		</div>
	</div>
	<div class="bot"></div>
</div>

<div class="neskolko-voprosov">
	<div class="container2">
		<h2>Есть сомнения?<br>Позвольте задать вам еще несколько вопросов...</h2>
		<div class="row">
			<div class="col-md-9">
				<iframe src="//player.vimeo.com/video/90304005" width="100%" height="440" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
			<div class="col-md-3">
				<div class="head-form">
					<div class="head-form-top">
						<p>Закажите полный курс</p>
					</div>
					<form action="" id="enter_form3" method="POST" class="main-form" onsubmit="yaCounter24857429.reachGoal('NIZFORMA'); return true;">
						<input type="hidden" name="send_enter_form" value="1" />
						<input type="text" id="name4" name="username" placeholder="Имя">
						<input type="text" id="email4" name="email" placeholder="E-mail">
						<input type="radio" id="r5" name="rr" value="electro" checked>
						<label for="r5"><span></span>Электронная версия</label>
						<input type="radio" id="r6" name="rr" value="cd">
						<label for="r6"><span></span>Версия на диске</label>
						<input type="submit" value="Заказать курс">
						<p>*Ваши данные в безопасности и не будут переданы третьим лицам.</p>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="header-top">
	<div class="container2">
		<div class="row">
			<div class="col-md-3">
				<img src="img/logo1.jpg" alt="" class="left-img">
			</div>
			<div class="col-md-6">
				<p>Ступенчатая система обучения инфобизнесу без начального капитала</p>
			</div>
			<div class="col-md-3">
				<img src="img/logo2.png" alt="" class="right-img">
			</div>
		</div>
	</div>
</div>

<div class="footer">
	<a href="" data-toggle="modal" data-target="#partner">Партнерская программа</a>
	<a href="" data-toggle="modal" data-target="#politica">Политика конфиденциальности</a>
	<a href="" data-toggle="modal" data-target="#contacts">Наши контакты</a>
	<p>Все права защищены. Copyright © 2013 Василий Купчихин.
		<br>ИП Купчихин Василий Васильевич ОГРН 312504010300040</p>
</div>

<?php include "modal.php"; ?>
<?php include "footer.php" ?>