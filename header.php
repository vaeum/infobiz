<?php include 'incl/settings.php'; ?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Инфобизнес</title>
	<meta name="description" content="Описание сайта">
	<meta name="Keywords" content="Ключевые слова">
	
	<link rel="icon" type="image/x-icon" href="img/favicon.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">

	<!--[if lte IE 6]><link rel="stylesheet" href="css/ie6.css"><![endif]-->
	<!--[if lte IE 7]><link rel="stylesheet" href="css/ie7.css"><![endif]-->
	<!--[if lte IE 8]><link rel="stylesheet" href="css/ie8.css"><![endif]-->
	<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css"><![endif]-->
	<script src="js/jquery.min.js"></script>
</head>
<body>