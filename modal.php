<!-- Модальное окно на первый отзыв -->
<div class="modal fade" id="otziv1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Константин Фёст</h4>
			</div>
			<div class="modal-body">
				<p>Учитывая имеющийся в курсе «Лучшие сервисы и программы для инфобизнеса» объем информации и ее качество, я считаю его одним из лучших вложений, которые сегодня может сделать начинающий интернет-предприниматель. 
					<br><br>Хороший обучающий курс от посредственного отличает то, что он дает необходимую информацию в полном объеме, но не перегружает пользователя лишней теорией. Это мы и видим здесь. 
					<br><br>Кроме того, новичок в интернет-бизнесе, зачастую, просто не знает, какие вообще на рынке существуют инструменты для ведения деятельности и повышения ее продуктивности. Поэтому то, что авторы провели большую работу по систематизации программ и сервисов, само по себе очень ценно. 
					<br><br>Я знаю Василия и Дмитрия не первый год, видел как они начинали выпускать свои обучающие продукты, поэтому могу объективно сказать, что этот курс является лучшим их продуктом по содержанию и качеству исполнения. 
					<br><br><a href="http://info-atlas.com">http://info-atlas.com</a> – Ваш путеводитель в мире обучающих курсов</p>
			</div>
		</div>
	</div>
</div>

<!-- Модальное окно на второй отзыв -->
<div class="modal fade" id="otziv2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Александр Гусев</h4>
			</div>
			<div class="modal-body">
				<p>Мне посоветовал данный видеокурс коллега. Я расширяю свой бизнес, поэтому нужны были знания, как привлечь клиентов, какие технические, маркетинговые моменты нужно учесть. В видео курсе нашел для себя все это. Действительно, очень много полезной информации. А также бесплатные бонусы очень порадовали.</p>
			</div>
		</div>
	</div>
</div>

<!-- Модальное окно на третий отзыв -->
<div class="modal fade" id="otziv3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Иван Макаров</h4>
			</div>
			<div class="modal-body">
				<p>Я был сильно мотивирован заработком 300 000 руб/мес. Были кредиты, которые сдавливали душу. Нужно был срочно что-то решать. И я рискнул купить курс Дмитрия и Василия и пахать по 25 часов в сутки! Нишу выбрал быстро. <br><br>Мне хотелось применить знания, монетизировать свою экспертность в нише юридического консалтинга, т.к. я юрист по образованию и практикую уже 6 лет. Я продал один из своих тренингов и два работающих сайта за 770 000 рублей!<br><br> При этом, я остаюсь в деле и продолжаю развивать и выводить на новый уровень мои знания. Уже без долгов и кредитов. Планирую активно применять и внедрять полученные навыки! Ну и как всегда, у меня еще куча проектов и планов!</p>
			</div>
		</div>
	</div>
</div>

<!-- Модальное окно на четвертый отзыв -->
<div class="modal fade" id="otziv4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Наталья Капустина</h4>
			</div>
			<div class="modal-body">
				<p>Я консультант и бизнес тренер. Заработано с тренинга 350 000 дохода за 6 месяцев работы. Оплата идет по 50 000 рублей в месяц. К тому же продан коучинг за 40 000 рублей и по 20,000 рублей две индивидуальные работы. Есть еще ряд мелких доходов с семинаров тысяч на 15-20, но их не подсчитывала.</p>
			</div>
		</div>
	</div>
</div>

<!-- Модальное окно подробнее -->
<div class="modal fade" id="сollapse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">«Лучшие сервисы для инфобизнеса»</h4>
			</div>
			<div class="modal-body">
				<div class="collapse-one">
					<div class="panel-group" id="accordion">
						<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
								<i class="head-num">01</i>Модуль по SmartResponder.ru<i class="caret pull-right"></i>
							</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse">
							<div class="panel-body">
							<ul>
								<li><i class="head-num">01</i>Зачем нужна рассылка?</li>
								<li><i class="head-num">02</i>Создание аккаунта на SmartResponder</li>
								<li><i class="head-num">03</i>Создание простой рассылки</li>
								<li><i class="head-num">04</i>Создание автоматической цепочки писем</li>
								<li><i class="head-num">05</i>Генератор форм подписки</li>
								<li><i class="head-num">06</i>Генератор всплывающих окон</li>
								<li><i class="head-num">07</i>Отправка письма по базе</li>
								<li><i class="head-num">08</i>Отправка HTML-письма по базе</li>
								<li><i class="head-num">09</i>Сбор и анализ статистики</li>
								<li><i class="head-num">10</i>Ссылки–счетчики и ROI</li>
								<li><i class="head-num">11</i>Каналы подписчиков</li>
								<li><i class="head-num">12</i>Опросы подписчиков</li>
								<li><i class="head-num">13</i>Сплит-тестирование писем</li>
								<li><i class="head-num">14</i>Создание гостевого аккаунта</li>
							</ul>
							</div>
						</div>
						</div>
						<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
								<i class="head-num">02</i>Сервис для создания интеллект-карт<i class="caret pull-right"></i>
							</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse">
							<div class="panel-body">
								<ul>
									<li><i class="head-num">01</i>Что такое интеллект-карта</li>
									<li><i class="head-num">02</i>Создание интеллект-карты</li>
								</ul>
							</div>
						</div>
						</div>
						<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
								<i class="head-num">03</i>Сервис делегирования полномочий<i class="caret pull-right"></i>
							</a>
							</h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse">
							<div class="panel-body">
								<ul>
									<li><i class="head-num">01</i>Что такое делегирование?</li>
									<li><i class="head-num">02</i>Регистрация на сайте Free-lance.ru</li>
									<li><i class="head-num">03</i>Создание нового проекта</li>
									<li><i class="head-num">04</i>Поиск ответственного исполнителя</li>
									<li><i class="head-num">05</i>Работа через сервис «Безопасная сделка»</li>
								</ul>
							</div>
						</div>
						</div>
						<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
								<i class="head-num">04</i>Сервис по подбору хостинга<i class="caret pull-right"></i>
							</a>
							</h4>
						</div>
						<div id="collapse4" class="panel-collapse collapse">
							<div class="panel-body">
								<ul>
									<li><i class="head-num">01</i>Работа с сайтом hosting-ninja.ru</li>
								</ul>
							</div>
						</div>
						</div>
						<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
								<i class="head-num">05</i>Доставка товара<i class="caret pull-right"></i>
							</a>
							</h4>
						</div>
						<div id="collapse5" class="panel-collapse collapse">
							<div class="panel-body">
								<ul>
									<li><i class="head-num">01</i>Обзор сервисов по доставке</li>
								</ul>
							</div>
						</div>
						</div>
						<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
								<i class="head-num">06</i>Справочно-информационный интернет-сервис<i class="caret pull-right"></i>
							</a>
							</h4>
						</div>
						<div id="collapse6" class="panel-collapse collapse">
							<div class="panel-body">
								<ul>
									<li><i class="head-num">01</i>Обзор портала</li>
									<li><i class="head-num">02</i>Регистрация и авторизация на портале</li>
									<li><i class="head-num">03</i>Словари и онлайн-проверка слов</li>
									<li><i class="head-num">04</i>Обзор электронных библиотек</li>
									<li><i class="head-num">05</i>Статьи о русском языке</li>
									<li><i class="head-num">06</i>Обзор раздела «Справка»</li>
									<li><i class="head-num">07</i>Обзор раздела «Класс»</li>
									<li><i class="head-num">08</i>Обзор раздела «Лента»</li>
									<li><i class="head-num">09</i>Обзор раздела «Игра»</li>
								</ul>
							</div>
						</div>
						</div>

						<p><i class="head-num">07</i>Сервис для распознавания текста</p>
						<p><i class="head-num">08</i>Сервис для создания красивых таблиц сравнения</p>
						<p><i class="head-num">09</i>Сервис для привлечения целевых посетителей</p>
						<p><i class="head-num">10</i>Социальная сеть для бизнеса</p>
						<p><i class="head-num">11</i>Генератор красивых целевых страниц</p>
						<p><i class="head-num">12</i>Сервис для ведения бухгалтерии онлайн «Моё дело»</p>
						<p><i class="head-num">13</i>Сервис онлайн-конвертирования файлов</p>
						<p><i class="head-num">14</i>Сервис планирования задач</p>
						<p><i class="head-num">15</i>Сервис создания маркетинговых опросов</p>

						<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
								<i class="head-num">16</i>3 сервиса онлайн-консультирования и поддержки<i class="caret pull-right"></i>
							</a>
							</h4>
						</div>
						<div id="collapse7" class="panel-collapse collapse">
							<div class="panel-body">
								<ul>
									<li><i class="head-num">01</i>Redhelper.ru</li>
									<li><i class="head-num">02</i>Reformal.ru</li>
									<li><i class="head-num">03</i>Support-desk.ru</li>
								</ul>
							</div>
						</div>
						</div>
						<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
								<i class="head-num">17</i>Модуль уроков по социальной сети Twitter.com<i class="caret pull-right"></i>
							</a>
							</h4>
						</div>
						<div id="collapse8" class="panel-collapse collapse">
							<div class="panel-body">
								<ul>
									<li><i class="head-num">01</i>Микроблоги на Twitter</li>
									<li><i class="head-num">02</i>Регистрация на сервисе</li>
									<li><i class="head-num">03</i>Интерфейс сервиса</li>
									<li><i class="head-num">04</i>Написание сообщений в Twitter</li>
									<li><i class="head-num">05</i>Списки контактов Twitter</li>
									<li><i class="head-num">06</i>Приложения для постинга в Twitter</li>
								</ul>
							</div>
						</div>
						</div>

						<p><i class="head-num">18</i>Сервис ввода-вывода электронных денег Webmoney</p>
						<p><i class="head-num">19</i>Биржа удалённой работы Workzilla</p>

						<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse9">
								<i class="head-num">20</i>Модуль уроков по видеохостингу YouTube.com<i class="caret pull-right"></i>
							</a>
							</h4>
						</div>
						<div id="collapse9" class="panel-collapse collapse">
							<div class="panel-body">
								<ul>
									<li><i class="head-num">01</i>Общие сведения о сервисе</li>
									<li><i class="head-num">02</i>Интерфейс сервиса</li>
									<li><i class="head-num">03</i>Поиск видео на сервисе</li>
									<li><i class="head-num">04</i>Регистрация на сервисе</li>
									<li><i class="head-num">05</i>Аккаунт пользователя</li>
									<li><i class="head-num">06</i>Канал пользователя</li>
									<li><i class="head-num">07</i>Подписка на каналы </li>
									<li><i class="head-num">08</i>Добавление видео на сервис</li>
									<li><i class="head-num">09</i>Комментарии на сервисе</li>
									<li><i class="head-num">10</i>Действия над видеороликами</li>
								</ul>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Модальное окно подробнее2 -->
<div class="modal fade" id="сollapse2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">«Лучшие программы для инфобизнеса»</h4>
			</div>
			<div class="modal-body">
				<div class="collapse-one">
					<div class="panel-group" id="accordion">

						<p><i class="head-num">01</i>Красивая программа для хранения паролей</p>
						<p><i class="head-num">02</i>Бесплатная программа для создания скриншотов</p>
						<p><i class="head-num">03</i>Программа для наведения порядка на рабочем столе</p>
						<p><i class="head-num">04</i>Лучший бесплатный FTP-менеджер</p>
						<p><i class="head-num">05</i>Пакет бесплатных красивейших мультимедийных программ</p>
						<p><i class="head-num">06</i>Универсальная программа для просмотра файлов</p>
						<p><i class="head-num">07</i>Бесплатная программа для отправки SMS и MMS</p>
						<p><i class="head-num">08</i>Программа-органайзер</p>
						<p><i class="head-num">09</i>Красивая программа для просмотра электронных книг</p>
						<p><i class="head-num">10</i>Красивая программа для создания профессиональных визиток</p>
						<p><i class="head-num">11</i>Стильный клиент-менеджер для Twitter</p>
						<p><i class="head-num">12</i>Универсальная программа для поиска плагиата</p>
						<p><i class="head-num">13</i>Программа, которая следит за раскладкой клавиатуры</p>
						<p><i class="head-num">14</i>Программа для восстановления данных</p>

						<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse10">
								<i class="head-num">15</i>Модуль уроков по программе Skype<i class="caret pull-right"></i>
							</a>
							</h4>
						</div>
						<div id="collapse10" class="panel-collapse collapse">
							<div class="panel-body">
								<ul>
									<li><i class="head-num">01</i>Регистрация в программе</li>
									<li><i class="head-num">02</i>Интерфейс программы</li>
									<li><i class="head-num">03</i>Работа с контактами</li>
									<li><i class="head-num">04</i>Настройки программы</li>
									<li><i class="head-num">05</i>Персональные настройки Skype и экскурс по вкладкам</li>
									<li><i class="head-num">06</i>Голосовая связь, видеосвязь и чат</li>
									<li><i class="head-num">07</i>Запись разговоров в Skype </li>
								</ul>
							</div>
						</div>
						</div>

						<p><i class="head-num">16</i>Программа-конвертер JPG в PDF</p>
						<p><i class="head-num">17</i>Программа для подсчёта времени, затраченного на работу над проектами</p>
						<p><i class="head-num">18</i>Красивая программа для создания интеллект-карт</p>
						<p><i class="head-num">19</i>Программа для создания скриншотов интернет-страниц</p>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Модальное окно наших контактов -->
<div class="modal fade" id="contacts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Наши контакты</h4>
			</div>
			<div class="modal-body">
				<ul>
					<li><b>Адрес:</b> г. Смоленск, улица М. Краснофлотская, дом 37Б</li>
					<li><b>Телефон для связи:</b> 8(800)333-39-75</li>
					<li><b>Skype:</b> ebay-consult</li>
					<li><b>E-mail:</b> atom333(собака)gmail.com</li>
					<li><b>Служба поддержки:</b> <a href="http://blog.pc-lessons.ru/support/">http://blog.pc-lessons.ru/support/</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<!-- Модальное окно политики конфедециальности -->
<div class="modal fade" id="politica" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Политика конфиденциальности</h4>
			</div>
			<div class="modal-body">
				<p>Администрация сайта <a href="http://programmbiz.ru">programmbiz.ru</a> ответственно относится к защите персональных данных.</p>
				<p>Мы уважаем доверие, которое Вы оказываете нам, предоставляя Вашу личную информацию.</p>
				<p>Мы хотели бы проинформировать Вас о том, какие именно данные мы собираем и каким образом их используем, а так же о том, как Вы можете дать нам указание об удалении подобной информации.</p>
				<em>Политика конфиденциальности</em>
				<p>Настоящая Политика конфиденциальности персональной информации (далее — Политика) действует в отношении любой информации, которую лица, составляющие администрацию и технический персонал сайта <a href="http://programmbiz.ru">programmbiz.ru</a> могут получить о пользователе во время использования последним сайта <a href="http://programmbiz.ru">programmbiz.ru</a></p>
				<p>Использование сайта <a href="http://programmbiz.ru">programmbiz.ru</a>, предоставляемых на нём сервисов, товаров и услуг, означает безоговорочное согласие пользователя с настоящей Политикой и указанными в ней условиями обработки его персональной информации; в случае несогласия с этими условиями пользователь должен воздержаться от использования сайта <a href="http://programmbiz.ru">programmbiz.ru</a> и предоставляемых на нём сервисов, товаров и услуг.</p>
				<p><b>1. Персональная информация пользователей, которую получает и обрабатывает сайт <a href="http://programmbiz.ru">programmbiz.ru</a></b></p>
				<p>1.1. В рамках настоящей Политики под «персональной информацией пользователя» понимаются:</p>
				<p>1.1.1. Личные данные пользователя, которые пользователь предоставляет о себе  самостоятельно при регистрации (создании учётной записи) или в процессе использования сервисов, включая персональные данные пользователя и(или) его ребёнка, достигшего 13-летнего возраста. Обязательная для предоставления сервисов (оказания услуг, приобретения и доставки товара) информация помечена звёздочкой. Иная информация предоставляется пользователем на его усмотрение.</p>
				<p>1.1.2 Данные, которые автоматически передаются сервисам сайта <a href="http://programmbiz.ru">programmbiz.ru</a> в процессе их использования с помощью установленного на устройстве пользователя программного обеспечения, в том числе IP-адрес, информация из cookie, информация о браузере пользователя, время доступа, адрес запрашиваемой страницы.</p>
				<p>1.2. Настоящая Политика применима только к сайту <a href="http://programmbiz.ru">programmbiz.ru</a>. Администрация не контролирует и не несет ответственность за сайты третьих лиц, на которые пользователь может перейти по ссылкам, доступным на сайтах <a href="http://programmbiz.ru">programmbiz.ru</a>.</p>
				<p>1.3. Администрация в общем случае не проверяет достоверность персональной информации, предоставляемой пользователями, и не осуществляет контроля за дееспособностью последних. Однако Администрация исходит из того, что пользователь предоставляет достоверную персональную информацию, не противоречащую правилам и условиям использования сайта <a href="http://programmbiz.ru">programmbiz.ru</a>.</p>
				<p><b>2. Цели сбора и обработки персональной информации пользователей</b></p>
				<p>2.1. Сайт <a href="http://programmbiz.ru">programmbiz.ru</a> собирает и хранит только те персональные данные, которые необходимы для приобретения и доставки товаров,  предоставления сервисов, оказания услуг  (исполнения соглашений и договоров с пользователем), а так же для оформления сопутствующих документов, коими могут быть: накладные, договоры, акты выполненных услуг, сертификаты, свидетельствующие о прохождении дистанционного обучения на сайте <a href="http://programmbiz.ru">programmbiz.ru</a></p>
				<p>2.2. Персональную информацию пользователя мы можем использовать в следующих целях:</p>
				<p>2.2.1. Идентификация стороны в рамках соглашений и договоров;</p>
				<p>2.2.2. Предоставление пользователю персонализированных сервисов;</p>
				<p>2.2.3. Связь с пользователем в случае необходимости, в том числе направление уведомлений, запросов и информации, связанных с использованием сервисов, оказанием услуг, а также обработка запросов и заявок от пользователя;</p>
				<p>2.2.4. Предоставление пользователю информации, связанной с выбранной им темой подписки или по программе учебного курса/тренинга:  информационного, либо обучающего характера; как в виде текстов, изображений, аудио и видео материалов или ссылок на них, пересылаемых по электронной почте, так и отправляемых обычной почтой на физических носителях бумажных или электронных;</p>
				<p>2.2.5. Улучшение качества сервисов, удобства их использования, разработка новых сервисов и услуг;</p>
				<p>2.2.6. Таргетирование рекламных материалов;</p>
				<p>2.2.7. Проведение статистических и иных исследований на основе обезличенных данных.</p>
				<p><b>3. Условия обработки персональной информации пользователя и её передачи третьим лицам</b></p>
				<p>3.1. В отношении персональной информации пользователя сохраняется ее конфиденциальность, кроме случаев добровольного предоставления пользователем информации о себе для общего доступа неограниченному кругу лиц. При использовании отдельных сервисов, пользователь соглашается с тем, что определённая часть его персональной информации становится общедоступной.</p>
				<p>3.3. Администрация сайта <a href="http://programmbiz.ru">programmbiz.ru</a> вправе передать персональную информацию пользователя третьим лицам в следующих случаях:</p>
				<p>3.3.1. Пользователь явно выразил свое согласие на такие действия;</p>
				<p>3.3.2. Передача необходима в рамках использования пользователем определенного сервиса либо для оказания услуги пользователю. При этом обеспечивается конфиденциальность персональной информации, а пользователь будет явным образом уведомлён о такой передаче;</p>
				<p>3.3.3. Передача предусмотрена российским или иным применимым законодательством в рамках установленной законодательством процедуры;</p>
				<p>3.3.4. Такая передача происходит в рамках продажи или иной передачи бизнеса (полностью или в части), при этом к приобретателю переходят все обязательства по соблюдению условий настоящей Политики применительно к полученной им персональной информации;</p>
				<p>3.3.5. В целях обеспечения возможности защиты прав и законных интересов владельцев сайта <a href="http://programmbiz.ru">programmbiz.ru</a> или третьих лиц в случаях, когда пользователь нарушает пользовательское соглашение сервисов сайта <a href="http://programmbiz.ru">programmbiz.ru</a>, или условия заключённого с ним Договора.</p>
				<p>3.4. При обработке персональных данных пользователей, администрация сайта <a href="http://programmbiz.ru">programmbiz.ru</a> руководствуется Федеральным законом РФ «О персональных данных».</p>
				<p><b>4. Удаление персональной информации</b></p>
				<p>Вы имеете право в любое время потребовать удаления информации о Вас из нашей базы данных. Вы также имеете право в любое время отозвать свое согласие на использование или обработку Ваших персональных данных, направив письмо в нашу службу поддержки - <a href="http://blog.pc-lessons.ru/support/">http://blog.pc-lessons.ru/support/</a></p>
				<p>Получая от нас письма по электронной почте в рамках подписки на учебный курс или по какой-то тематике, Вы можете самостоятельно отписаться от такой рассылки нажатием на ссылку «Отписаться» в нижней части сообщения. Ваши персональные данные будут удалены из нашей базы данных, если только Вы не подписаны на другие рассылки или дистанционные курсы, от которых, по своему желанию, Вы можете отписаться таким же образом.</p>
				<p>Удаление персональных данных из нашей базы данных, может повлечь за собой невозможность использовать на сайте определённые сервисы и услуги, приобретать товар.</p>
				<p>5. Меры, применяемые для защиты персональной информации пользователей</p>
				<p>Мы принимаем необходимые и достаточные организационные и технические меры для защиты персональной информации пользователя от неправомерного или случайного доступа, уничтожения, изменения, блокирования, копирования, распространения, а также от иных неправомерных действий с ней третьих лиц.</p>
				<p><b>6. Изменение Политики конфиденциальности</b></p>
				<p>6.1. Мы имеем право вносить изменения в настоящую Политику конфиденциальности. При внесении изменений в актуальной редакции указывается дата последнего обновления. Новая редакция Политики вступает в силу с момента ее размещения, если иное не предусмотрено новой редакцией Политики. Действующая редакция всегда находится на странице по адресу <a href="http://programmbiz.ru/privacy.html">http://programmbiz.ru/privacy.html</a></p>
				<p>6.2. К отношениям между пользователем и Администрацией сайта <a href="http://programmbiz.ru">programmbiz.ru</a>, возникающим в связи с применением Политики конфиденциальности, подлежит применению право Российской Федерации.</p>
				<p><b>7. Обратная связь. Вопросы и предложения</b></p>
				<p>Предложения и вопросы по поводу настоящей Политики следует сообщать в нашу службу поддержки - <a href="http://blog.pc-lessons.ru/support/">http://blog.pc-lessons.ru/support/</a></p>
			</div>
		</div>
	</div>
</div>

<!-- Модальное окно наша партнерская программа -->
<div class="modal fade" id="partner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Партнерская программа</h4>
			</div>
			<div class="modal-body">
				<em>Зарабатывайте от 920 рублей до 1196 рублей с каждой проданной через Вас копии курса «Лучшие сервисы и программы для инфобизнеса»</em>
				<!-- <img src="img/cover.jpg" alt=""> -->
				<i>Как работает партнёрская программа?</i>
				<p>1) Вы <a href="http://programmbiz.e-autopay.com/affreg/">регистрируетесь в партнёрской программе</a>, указывая свои данные и номера своих электронных кошельков для перевода комиссионных вознаграждений. По окончании регистрации, в личном кабинете партнёра, Вы обнаружите партнёрские ссылки на курс «Лучшие сервисы и программы для инфобизнеса». Просто выбираете её, копируете и можно приступать к рекламе.</p>
				<p>Как правило, продвижение курса партнёрами осуществляется посредством своего сайта, контекстной рекламы (Яндекс Директ, Google Adwords, таргетинговая реклама Вконтакте, реклама в тизерных сетях и так далее), рекламы в рассылках. <b>Главное соблюдать одно простое правило – НИКАКОГО СПАМА!</b></p>
				<p>2) Посетители будут кликать по рекламе и переходить на продающую страничку курса по Вашей партнёрской ссылке.</p>
				<p>3) Если человек, который пришёл от Вас, совершит заказ и оплатит его, то Вы получите на свой электронный кошелек комиссионное вознаграждение: <b>920 рублей</b> (если клиент приобрел по Вашей партнерской ссылке электронную версию курса) и <b>1196</b> рублей (если клиент приобрел по Вашей партнерской ссылке физическую версию курса).</p>
				<p>4) Отследить всю статистику по переходам и продажам, узнать, сколько Вы заработали, а также заказать деньги к выплате, Вы сможете в своём личном кабинете партнёра.</p>
				<i>Требования к партнёрам</i>
				<p>1) При регистрации в партнёрской программе Вы должны указать <b>Ваш рабочий e-mail адрес</b></p>
				<p>2) Категорически запрещено использовать <b>СПАМ</b> и другие нелегальные способы рекламы Вашей партнёрской ссылки. В противном случае Ваш аккаунт будет моментально заблокирован <b>БЕЗ ВЫПЛАТЫ КОМИССИОННОГО ВОЗНАГРАЖДЕНИЯ</b> (такие случаи уже были).</p>
				<p>3) Вы не должны пытаться приобрести курс по Вашей партнёрской ссылке.</p>
				<p>4) Вы должны рекламировать СВОЮ ПАРТНЕРСКУЮ ССЫЛКУ, чтобы за Вами засчитывались заказы курса. Ссылка будет иметь следующий вид (её Вы сможете найти, зайдя в свой партнёрский аккаунт и перейдя на вкладку «Ваши ссылки»):</p>
				<p>http://programmbiz.ru/<span>Ваш ID</span></p>
				<p><b>Успешных Вам продаж!</b></p>
				<a href="http://programmbiz.e-autopay.com/affreg/" class="begin">НАЧАТЬ РЕГИСТРАЦИЮ В ПАРТНЁРСКОЙ ПРОГРАММЕ</a>
			</div>
		</div>
	</div>
</div>